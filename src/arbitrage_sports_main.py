'''
This will be a webscraper that keeps track of all sporting events on each day.

Author: Jeffrey Plett
Date: March 23, 2020
'''
import os

from bookies.SportsInteraction import SportsInteractionScraper
from bookies.Betway import BetwayScraper
from db_interface import DBInterface

import logging

logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
logger = logging.getLogger('logger')


def data_collection():
    all_games = []
    db = DBInterface()

    # Get games from Sports Interaction
    si = SportsInteractionScraper()
    si_odds = si.get_moneyline_odds()
    all_games.extend(si_odds)
    for game in si_odds:
        db.insert_game(game)

    # Get games from Betway
    bw = BetwayScraper()
    bw_odds = bw.get_moneyline_odds()
    all_games.extend(bw_odds)
    for game in bw_odds:
        db.insert_game(game)

    return all_games


if __name__ == "__main__":
    print('start')
    data_collection()
    print('end')
