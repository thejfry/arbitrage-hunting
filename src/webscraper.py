from selenium import webdriver
import time


class Webscraper:

    def __init__(self):
        self.driver = None

    def start_webdriver(self):
        self.driver = webdriver.Edge(r"C:\Program Files (x86)\Microsoft\edgedriver_win64\msedgedriver.exe")

    def restart_webdriver(self):
        self.quit()
        self.start_webdriver()

    def load_page(self, url):
        self.driver.get(url)
        time.sleep(6)  # todo: there must be a better way to check if the page is loaded here

    def close_browser(self):
        self.driver.close()

    def quit(self):
        self.driver.quit()
