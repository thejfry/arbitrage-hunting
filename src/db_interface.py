from pymongo import MongoClient
from dataclasses import asdict


class DBInterface:
    def __init__(self, host='localhost', port=27017):
        self.dbclient = MongoClient(host=host, port=port)
        self.db = self.dbclient.get_database('data')
        self.games = self.db.get_collection('games')

    def insert_game(self, game):
        game_dict = asdict(game)
        self.games.insert_one(game_dict)

    def get_games(self):
        return self.games.find()
