from dataclasses import dataclass, field
from uuid import UUID, uuid4
from datetime import datetime
from typing import List

from bookies.Bookkeeper import MoneylineOdds


@dataclass
class Game:
    id: UUID = field(default=None, compare=False)
    date: datetime = field(default=None)
    start_time: str = field(default=None, compare=False)
    league: str = field(default=None)
    home_team: str = field(default=None)
    visiting_team: str = field(default=None)
    moneyline_odds: List[MoneylineOdds] = field(default_factory=list, compare=False)

    def generate_id(self):
        if self.id is None:
            self.id = uuid4()
