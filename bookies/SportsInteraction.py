from bookies.Bookkeeper import BookKeeper, MoneylineOdds
from src.game import Game

from typing import List
from enum import Enum
from datetime import datetime
from selenium.webdriver.common.by import By
import logging

logger = logging.getLogger('logger')


class DOWMap(Enum):
    MON = 0
    TUE = 1
    WED = 2
    THU = 3
    FRI = 4
    SAT = 5
    SUN = 6


class SportsInteractionScraper(BookKeeper):
    def __init__(self):
        super().__init__()
        self.name = "Sports Interaction"
        self.home = 'https://www.sportsinteraction.com/'
        self.league_urls = {
            'NHL': 'hockey/nhl',
            'NBA': 'basketball/nba',
            'NFL': 'football/nfl',
        }
        self.date_string_format = '%a %b %d'

    def convert_date_string_to_datetime(self, date_string) -> datetime:
        """Creating datetime object out of date strings that commonly appear on Sports Interaction

            Args:
                date_string: the date string

            Returns:
                date: date as a datetime object
        """
        date = datetime.strptime(date_string, self.date_string_format)
        dow = date_string.split(' ')[0]
        dow = DOWMap[dow].value
        for i in range(2):  # there are only odds for games less than a year away
            date = date.replace(year=datetime.now().year + i)
            if date.weekday() == dow:
                break
        return date

    def get_moneyline_odds_for_league(self, league: str) -> List[Game]:
        logger.info(f'Getting odds from SportsInteraction for {league}')
        self.start_webdriver()
        self.load_page(f'{self.home}{self.league_urls[league]}-betting-lines/?betting_type_name=Moneyline')
        league_games = []

        date_panels = self.driver.find_elements(By.CLASS_NAME, 'GameDateGroup')
        for date_panel in date_panels:

            date_string = date_panel.find_element(By.CLASS_NAME,
                                                  "base-1.Heading.GameDateGroup__gameDate").text
            date = self.convert_date_string_to_datetime(date_string)

            game_elements = date_panel.find_elements(By.CLASS_NAME, "Game--listPage.Game")
            for game_element in game_elements:
                g = Game(date=date, league=league)
                g.start_time = game_element.find_element(By.CLASS_NAME,
                                                         "base-1.Paragraph.GameHeader__time.GameHeader__time--noLink").text
                teams = []
                teams_elements = game_element.find_elements(By.CLASS_NAME, "BetButton--row.BetButton--live.BetButton")
                if 'BetButton--closed' in teams_elements[0].get_attribute('class') or \
                        'BetButton--closed' in teams_elements[1].get_attribute('class'):
                    continue

                for team_element in teams_elements:
                    team_name = team_element.find_element(By.CLASS_NAME, "BetButton__runnerName").text
                    team_price = team_element.find_element(By.CLASS_NAME, "Odd.BetButton__price").text
                    teams.append({'team': team_name, 'price': team_price})

                if len(teams) == 2:
                    g.visiting_team = teams[0]['team']
                    g.home_team = teams[1]['team']

                # todo: check if g exists in games database. If not, add it.

                # Add the odds for the game to game object
                odds = MoneylineOdds(
                    bookkeeper=self.name,
                    visiting_team_odds=teams[0]['price'],
                    home_team_odds=teams[1]['price']
                )

                # todo: check if this bookmaker currently has odds for the game, if so decide whether to overwrite
                g.moneyline_odds.append(odds)
                league_games.append(g)
        self.quit()
        return league_games  # todo: settle on a better interface that this function can return the games in

    def get_moneyline_odds(self) -> List[Game]:
        bookkeepers_games = []
        for league in self.league_urls.keys():
            bookkeepers_games.extend(self.get_moneyline_odds_for_league(league))
        return bookkeepers_games

    def get_supported_leagues(self):
        return self.league_urls.keys()
