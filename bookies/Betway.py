from bookies.Bookkeeper import BookKeeper, MoneylineOdds
from src.game import Game

from typing import List
from datetime import datetime
from selenium.webdriver.common.by import By
import logging

logger = logging.getLogger('logger')


class BetwayScraper(BookKeeper):

    def __init__(self):
        super().__init__()
        self.name = "Betway"
        self.home = 'https://betway.com/en/sports'
        self.league_urls = {
            'NHL': '/grp/ice-hockey/north-america/nhl',
            'NFL': '/cat/american-football'
        }
        self.date_string_format = '%Y-%m-%d'

    def convert_date_string_to_datetime(self, date_string) -> datetime:
        """Creating datetime object out of date strings that commonly appear on Betway

            Args:
                date_string: the date string

            Returns:
                date: date as a datetime object
        """
        date = datetime.strptime(date_string, self.date_string_format)
        return date

    def get_moneyline_odds_for_league(self, league: str) -> List[Game]:
        logger.info(f'Getting odds from Betway for {league}')
        self.start_webdriver()
        self.load_page(f'{self.home}{self.league_urls[league]}')
        league_games = []

        # ensure the correct type of odds are being shown
        market_dropdown = self.driver.find_element(By.CLASS_NAME,
                                                   'dropdownContainer.dropdownStandard.marketFilterOuterContainer')
        moneyline_option_text = 'Money Line'
        if moneyline_option_text not in market_dropdown.find_element(By.CLASS_NAME, 'dropdownSelectedOptionText').text:
            dropdown_arrows = market_dropdown.find_element(By.CLASS_NAME, 'iconHolder.icon-dropdownarrows')
            dropdown_arrows.click()
            moneyline_option = market_dropdown.find_element(By.PARTIAL_LINK_TEXT, moneyline_option_text)
            moneyline_option.click()

        event_list = self.driver.find_element(By.CLASS_NAME, 'mainContent')
        date_panels = event_list.find_elements(By.CLASS_NAME, 'collapsablePanel')
        for date_panel in date_panels:
            date_string = date_panel.get_attribute('collectionitem').split('_')[-1]
            date = self.convert_date_string_to_datetime(date_string)

            games = date_panel.find_elements(By.CLASS_NAME, 'oneLineEventItem')
            for game in games:
                # this condition exists if there is an event in the panel that isn't a regular game
                if game.find_element(By.CLASS_NAME, 'eventName').text:
                    continue

                g = Game(date=date, league=league)
                g.start_time = game.find_element(By.CLASS_NAME, 'oneLineDateTime').text
                g.visiting_team = game.find_element(By.CLASS_NAME,
                                                    'teamNameFirstPart.teamNameHomeTextFirstPart').text
                g.home_team = game.find_element(By.CLASS_NAME,
                                                'teamNameFirstPart.teamNameAwayTextFirstPart').text
                # todo: check if g exists in games database. If not, add it.

                odds = game.find_elements(By.CLASS_NAME,
                                          'odds')  # todo: bug if there is a game panel that just shows "More Bets"
                away_team_odds = odds[0].text
                home_team_odds = odds[1].text

                odds = MoneylineOdds(
                    bookkeeper=self.name,
                    visiting_team_odds=away_team_odds,
                    home_team_odds=home_team_odds
                )

                # todo: check if this bookmaker currently has odds for the game, if so decide whether to overwrite
                g.moneyline_odds.append(odds)
                league_games.append(g)
        self.quit()
        return league_games  # todo: settle on a better interface that this function can return the games in

    def get_moneyline_odds(self) -> List[Game]:
        bookkeepers_games = []
        for league in self.league_urls.keys():
            bookkeepers_games.extend(self.get_moneyline_odds_for_league(league))
        return bookkeepers_games

    def get_supported_leagues(self):
        return self.league_urls.keys()
