from abc import abstractmethod
from dataclasses import dataclass
from uuid import UUID, uuid4

from src.webscraper import Webscraper


@dataclass
class MoneylineOdds:
    id: UUID = None
    bookkeeper: str = None
    home_team_odds: float = None
    visiting_team_odds: float = None

    def generate_id(self):
        if self.id is None:
            self.id = uuid4()


class BookKeeper(Webscraper):
    def __init__(self):
        super().__init__()
        self.name = ""
        self.home = None

    @abstractmethod
    def get_moneyline_odds(self):
        '''This function shall return all moneyline odds a boookkeeper has for a given day'''
        pass

    def save_odds(self, games):
        '''Save the bookkeepers odds in a database table

        Args:
        games (list <MoneylineOdds>): List of Game objects to be saved in a database
        '''
        pass
